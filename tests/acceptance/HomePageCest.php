<?php

use PHPUnit\Framework\Assert;


class HomePageCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/en');
    }

    // tests
    public function menuIsDisplayed(AcceptanceTester $I)
    {
        $I->see('Exhibits', 'li');
        $I->see('Boutique', 'li');
        $I->see('Learn', 'li');
        $I->see('About Us', 'li');
        $I->see('Support', 'li');
        $I->see('What\'s Happening', 'li');
    }

    public function exhibitsPreviewsAreListed(AcceptanceTester $I)
    {
        $I->seeNumberOfElements(['css' => 'div.exhibit'], 4);
    }

    public function postPreviewsAreListed(AcceptanceTester $I)
    {
        $I->seeNumberOfElements(['css' => 'div.post'], 4);
    }

    public function socialMediaIconsAreVisible(AcceptanceTester $I)
    {
        $I->seeElement('.fa-facebook-f');
        $I->seeElement('.fa-twitter');
        $I->seeElement('.fa-youtube');
    }

    public function sliderIsVisible(AcceptanceTester $I)
    {
        $I->expect("To see the slider text");
        $I->see("This is the content I expect to see", "p");

        $I->expect("To see the a feature image");

        $value = $I->grabAttributeFrom(
            '//body/section/div[1]',
            "style");

        Assert::assertTrue(
            strpos($value, "background-image: url('/user/pages/sliders/0.testing-slider/featureimage.jpg')") == 0,
            "Slider doesn't seem to have an image"
        );

        $I->expect("To see the slider to have a clickable link");
        $value = $I->grabAttributeFrom(
            '//body/section/div[1]',
            "onclick");

        Assert::assertTrue(
            strpos($value, "location.href='http://aurlthatwillgosomewhere.com'") == 0,
            "Slider doesn't have a clickable url under page.metadata[\"link\"]"
        );
    }

    public function languageSelectorIsVisible(AcceptanceTester $I)
    {
        $I->expect("To see the an English language selector");
        $I->see("English", "a");
        $I->expect("To see the an French language selector");
        $I->see("Français", "a");
        $I->expect("The English language selector has an 'active' class");
        $I->seeNumberOfElements(['css' => 'a.external.active'], 1);
    }
}
