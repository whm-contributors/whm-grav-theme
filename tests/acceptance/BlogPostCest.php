<?php

use PHPUnit\Framework\Assert;


class BlogPostCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/en/whats_happening/testpost1');
    }

    // tests
    public function blogPostTitleIsDisplayed(AcceptanceTester $I)
    {
        $value = $I->grabTextFrom('//body/section[1]/div/h1');
        Assert::assertTrue(
            "Test title 1" == trim($value),
            "Blog post doesn't seem to have a title"
        );
    }

    public function blogPostTitleIsCentered(AcceptanceTester $I)
    {
        $value = $I->grabAttributeFrom(
                                '//body/section[1]/div/h1',
                                "style"
                            );
        Assert::assertTrue(
            strpos(trim($value), "text-align: center") > -1,
            "Blog post title is not centered"
        );
    }

    public function blogPostDateIsDisplayed(AcceptanceTester $I)
    {
        $value = $I->grabTextFrom('//body/section[2]/div/div/div[1]/span[1]');
        Assert::assertTrue(
            "May 1st, 2018" == trim($value),
            "Blog post date isn't displayed"
        );
    }

    public function blogPostReadTimeIsDisplayed(AcceptanceTester $I)
    {
        $value = $I->grabTextFrom('//body/section[2]/div/div/div[1]/span[2]');
        Assert::assertTrue(
            "3 Minutes" == trim($value),
            "Blog post doesn't seem to have a read time"
        );
    }

    public function blogPostTagsAreDisplayed(AcceptanceTester $I)
    {
        $value = $I->grabTextFrom('//body/section[2]/div/div/div[2]/ul/li[1]');
        Assert::assertTrue(
            "First" == $value,
            "Blog post doesn't display tags"
        );
        $value = $I->grabTextFrom('//body/section[2]/div/div/div[2]/ul/li[2]');
        Assert::assertTrue(
            "Second" == $value,
            "Blog post doesn't display tags"
        );
        $value = $I->grabTextFrom('//body/section[2]/div/div/div[2]/ul/li[3]');
        Assert::assertTrue(
            "Third" == $value,
            "Blog post doesn't display tags"
        );
    }
}
