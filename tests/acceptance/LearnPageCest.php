<?php


class LearnPageCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('Learn');
    }

    // tests
    public function isSideMenuVisible(AcceptanceTester $I)
    {
        $I->seeElement('.side-menu');
    }
}
