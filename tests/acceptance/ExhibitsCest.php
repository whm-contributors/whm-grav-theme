<?php

use PHPUnit\Framework\Assert;


class ExhibitsCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/en/exhibits');
    }

    // tests
    public function fourExhibitPreviewsAreDisplayed(AcceptanceTester $I)
    {
        $I->seeNumberOfElements(['css' => 'div.exhibit'], 5);
    }

    public function allExhibitsAreDisplayedAsExpected(AcceptanceTester $I)
    {
        $I->see('Test title 1', 'h3');
        $I->see('Test title 2', 'h3');
        $I->see('Test title 3', 'h3');
        $I->see('Test title 4', 'h3');
    }

    public function EnsureAllExhibitPreviewElementsArePresent(AcceptanceTester $I){
        $I->expect("The exhibit preview will have a feature image");
        $value = $I->grabAttributeFrom(
            '//body/section[2]/div/div/div[1]/div',
            "class");

        Assert::assertTrue(
            "feature-image" == $value,
            "Exhibit preview tile doesn't seem to have a feature image"
        );

        $I->expect("The exhibit preview will have a title");
        $value = $I->grabAttributeFrom(
            '//body//section[2]//div//div//div[1]//h3',
            "style");

        Assert::assertTrue(
            "text-transform: uppercase" == $value,
            "Exhibit preview tile doesn't seem to have a title"
        );

        $I->expect("The exhibit preview will have a synopsis");
        $value = $I->grabTextFrom('//body//section[2]//div//div//div[1]//p');

        print("MGDEV xx" . $value . "xxx");

        Assert::assertTrue(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus interdum tincidunt est, ac imperdiet eros sagittis nec. Phasellus pretiu
    " == $value,
            "Exhibit preview tile doesn't seem to have a synopsis"
        );

    }
}
