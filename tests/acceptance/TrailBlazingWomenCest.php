<?php

use PHPUnit\Framework\Assert;


class TrailBlazingWomenCest
{
    // tests
    public function englishBackToExhibitLinkWorks(AcceptanceTester $I)
    {
        $I->amOnPage("/en/exhibits/trailblazing-canadian-women");
        $I->click('English Bio');
        $I->click('Return to Exhibit');
        $I->see('English Bio', 'a');

    }

    public function frenchBackToExhibitLinkWorks(AcceptanceTester $I)
    {
        $I->amOnPage("/fr/exhibits/trailblazing-canadian-women");
        $I->click('French Bio');
        $I->click('Return to Exhibit');
        $I->see('French Bio', 'a');

    }
}
