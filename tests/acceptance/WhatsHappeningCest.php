<?php

use PHPUnit\Framework\Assert;

class WhatsHappeningCest
{


    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/whats_happening');
    }

    // tests
    public function eightFullPostPreviewsAreDisplayed(AcceptanceTester $I)
    {
        $I->seeNumberOfElements(['css' => 'section.blog-posts-list div.post,full'], 8);
    }

    public function twelveSmallPostPreviewsAreDisplayed(AcceptanceTester $I)
    {
        $I->seeNumberOfElements(['css' => 'section.older-blog-posts div.post,small'], 12);
    }

    public function paginationHasDifferntStyleForCurrentPage(AcceptanceTester $I)
    {
        $I->seeElement('li.current-page');
    }

    public function threePaginationOptionsShouldBeAvailable(AcceptanceTester $I)
    {
        $I->seeLink("1");
        $I->seeLink("2");
        $I->seeLink("3");
    }

    public function allPostsAreDisplayedAsExpectedThroughPagination(AcceptanceTester $I)
    {
        // All post we would expect to see on page 3
        $I->see('Test title 41', 'h3');
        $I->see('Test title 42', 'h3');
        $I->see('Test title 43', 'h3');
        $I->see('Test title 44', 'h3');
        $I->see('Test title 45', 'h3');
        $I->see('Test title 46', 'h3');
        $I->see('Test title 47', 'h3');
        $I->see('Test title 48', 'h3');
        $I->see('Test title 49', 'h3');
        $I->see('Test title 50', 'h3');
        $I->see('Test title 51', 'h3');
        $I->see('Test title 52', 'h3');
        $I->see('Test title 53', 'h3');
        $I->see('Test title 54', 'h3');
        $I->see('Test title 55', 'h3');
        $I->see('Test title 56', 'h3');
        $I->see('Test title 57', 'h3');
        $I->see('Test title 58', 'h3');
        $I->see('Test title 59', 'h3');
        $I->see('Test title 60', 'h3');

        $I->click('2');

        // All post we would expect to see on page 2
        $I->see('Test title 21', 'h3');
        $I->see('Test title 22', 'h3');
        $I->see('Test title 23', 'h3');
        $I->see('Test title 24', 'h3');
        $I->see('Test title 25', 'h3');
        $I->see('Test title 26', 'h3');
        $I->see('Test title 27', 'h3');
        $I->see('Test title 28', 'h3');
        $I->see('Test title 29', 'h3');
        $I->see('Test title 30', 'h3');
        $I->see('Test title 31', 'h3');
        $I->see('Test title 32', 'h3');
        $I->see('Test title 33', 'h3');
        $I->see('Test title 34', 'h3');
        $I->see('Test title 35', 'h3');
        $I->see('Test title 36', 'h3');
        $I->see('Test title 37', 'h3');
        $I->see('Test title 38', 'h3');
        $I->see('Test title 39', 'h3');
        $I->see('Test title 40', 'h3');

        $I->click('3');
                // All post we would expect to see on page 1
        $I->see('Test title 1', 'h3');
        $I->see('Test title 2', 'h3');
        $I->see('Test title 3', 'h3');
        $I->see('Test title 4', 'h3');
        $I->see('Test title 5', 'h3');
        $I->see('Test title 6', 'h3');
        $I->see('Test title 7', 'h3');
        $I->see('Test title 8', 'h3');
        $I->see('Test title 9', 'h3');
        $I->see('Test title 10', 'h3');
        $I->see('Test title 11', 'h3');
        $I->see('Test title 12', 'h3');
        $I->see('Test title 13', 'h3');
        $I->see('Test title 14', 'h3');
        $I->see('Test title 15', 'h3');
        $I->see('Test title 16', 'h3');
        $I->see('Test title 17', 'h3');
        $I->see('Test title 18', 'h3');
        $I->see('Test title 19', 'h3');
        $I->see('Test title 20', 'h3');

        
    }

    public function pageTitleIsDisplayedWithPageNumber(AcceptanceTester $I)
    {
        $I->see("What's Happening", 'h1');
        $I->click('2');
        $I->see("What's Happening - Page 2", 'h1');
        $I->click('3');
        $I->see("What's Happening - Page 3", 'h1');
    }


    public function nonFirstPagesShouldOnlyHaveSmallBlogPreviews(AcceptanceTester $I)
    {
        $I->click('2');
        $I->seeNumberOfElements(['css' => 'section.older-blog-posts div.post,small'], 20);
    }

    public function EnsureAllFullPostPreviewElementsArePresent(AcceptanceTester $I){
        $I->expect("The blog preview will have a feature image");
        $value = $I->grabAttributeFrom(
            '//body/section[2]/div/section[1]/div[1]/div',
            "class");

        Assert::assertTrue(
            "feature-image" == $value,
            "Blog preview tile doesn't seem to have a feature image"
        );

        $I->expect("The blog preview will have a synopsis");
        $value = $I->grabTextFrom('//body/section[2]/div/section[1]/div[1]/p');

        Assert::assertTrue(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus interdum tincidunt est, ac imperdiet eros sagittis nec. Phasellus pretium efficitur lobortis. Duis vel consectetur nibh. Integer sed urna in mi vulputate hendrerit. Suspendisse lacus metus, condimentum eu placerat at, rutrum ac quam. P..." == $value,
            "Blog preview tile doesn't seem to have a synopsis"
        );

        $I->expect("The blog preview will have a date");
        $value = $I->grabTextFrom('//body/section[2]/div/section[1]/div[1]/span');

        Assert::assertTrue(
            "June 30th, 2018" == $value,
            "Blog preview tile doesn't seem to have a date"
        );

        $I->expect("The blog preview will have a title");
        $value = $I->grabTextFrom('//body/section[2]/div/section[1]/div[1]/h3');

        Assert::assertTrue(
            "Test title 60" == $value,
            "Blog preview tile doesn't seem to have a title"
        );

    }

    public function EnsureAllSmallPostPreviewElementsArePresent(AcceptanceTester $I){
        $I->expect("The blog preview will have a feature image");
        $value = $I->grabAttributeFrom(
            '//body/section[2]/div/section[2]/div/div[1]/div',
            "class");

        Assert::assertTrue(
            "feature-image" == $value,
            "Blog preview tile doesn't seem to have a feature image"
        );

        $I->expect("The blog preview will have a date");
        $value = $I->grabTextFrom('//body/section[2]/div/section[2]/div/div[1]/span');

        Assert::assertTrue(
            "June 22th, 2018" == $value,
            "Blog preview tile doesn't seem to have a dates"
        );

        $I->expect("The blog preview will have a title");
        $value = $I->grabTextFrom('//body/section[2]/div/section[2]/div/div[1]/h3');

        Assert::assertTrue(
            "Test title 52" == $value,
            "Blog preview tile doesn't seem to have a title"
        );

        $I->expect("The blog preview will NOT have a text summary");
        $I->dontSeeElement('//body/section[2]/div/section[2]/div/div[1]/div/div[2]/p');
    }
}
