cp ./pages/04.learn/research/vertical_menu_left_with_content_right.en.md ./pages/04.learn/research/vertical_menu_left_with_content_right.fr.md
cp ./pages/06.about_us/01.history-of-the-museum/vertical_menu_left_with_content_right.en.md ./pages/06.about_us/01.history-of-the-museum/vertical_menu_left_with_content_right.fr.md
cp ./pages/06.about_us/02.logos/vertical_menu_left_with_content_right.en.md ./pages/06.about_us/02.logos/vertical_menu_left_with_content_right.fr.md
cp ./pages/06.about_us/03.annual-reports/vertical_menu_left_with_content_right.en.md ./pages/06.about_us/03.annual-reports/vertical_menu_left_with_content_right.fr.md
cp ./pages/06.about_us/04.board-of-directors/vertical_menu_left_with_content_right.en.md ./pages/06.about_us/04.board-of-directors/vertical_menu_left_with_content_right.fr.md
cp ./pages/06.about_us/05.collections/vertical_menu_left_with_content_right.en.md ./pages/06.about_us/05.collections/vertical_menu_left_with_content_right.fr.md
cp ./pages/06.about_us/06.mandate-and-by-laws/vertical_menu_left_with_content_right.en.md ./pages/06.about_us/06.mandate-and-by-laws/vertical_menu_left_with_content_right.fr.md
cp ./pages/06.about_us/07.patrons-and-advisors/vertical_menu_left_with_content_right.en.md ./pages/06.about_us/07.patrons-and-advisors/vertical_menu_left_with_content_right.fr.md