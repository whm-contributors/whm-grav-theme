[![Gitter chat](https://badges.gitter.im/gitterHQ/gitter.png)](https://gitter.im/Workers-History-Museum/Lobby) [![pipeline status](https://gitlab.com/whm-contributors/whm-grav-theme/badges/master/pipeline.svg)](https://gitlab.com/whm-contributors/whm-grav-theme/commits/master)

# Workers History Museum
The Workers History Museum is an Ottawa, Canada based not-for-profit dedicated preserving and promoting workers' history in the National Capital Region and Ottawa Valley

## What are these files?
 `docker`                   - provisioning files for our Docker Image  
 `pages`                    - all the webpages renederd by Grav  
 `workers-history-museum`   - files that comprise a [Grav](https://getgrav.org/) theme  
 `scripts`                  - scripts for running tests, backing up and setting up development environment (also included in Docker container)  
 `tests`                    - all tests relating to the codebase

## Contributing
We are open to anyone submitting fixes and issues relating to [www.workershistorymuseum.ca](http://workershistorymuseum.ca). Simply clone our repo and create a PR for the project owner to merge.

# Setup
The following steps have been verified using Linux. Non Linux devs: run a Linux environment inside Virtual Machine.

## Prerequisites 
*  Docker should be installed and running on your host
*  You should have [docker-compose](https://github.com/docker/compose) installed
*  NPM
*  curl

## Clone the repo
```
git clone https://gitlab.com/micealgallagher/whm-grav-theme.git
```

Install dependencies
```
npm install
```

Start the development environment
```
npm run start
```

Now navigate to [http://localhost:80](http://localhost:80)

Restore most recent backup to local (optional)
```
npm run setup
```

# Common Problems

## Port conflict
When encountering the following issue:
```
Bind for 0.0.0.0:80 failed: port is already allocated'
```
Modify the `ports` of `docker-compose.yml` to work with an unused port
```
...
ports:
    - "8080:80" // now we'll be using http://localhost:8080 instead
...
```

## Permissions
Allow writing on host and reading/executing on container
```
sudo chown -R `whoami`:`whoami` ./pages
sudo chown -R `whoami`:`whoami` ./workers-history-museum
```
