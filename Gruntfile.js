module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'workers-history-museum/css/main.css': 'workers-history-museum/css/main.scss'
                }
            }
        },
        watch: {
            css: {
                files: [
                    'workers-history-museum/css/*.scss',
                    'workers-history-museum/css/partials/*.scss',
                    'workers-history-museum/css/partials/pages/*.scss'
                ],
                tasks: ['sass'],
            },
        },
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
};
