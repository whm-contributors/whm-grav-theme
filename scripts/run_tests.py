import os
import sys
import shutil
import subprocess
import time

CODE_ROOT = "%s" %(os.getcwd())
CODE_TEST_FIXTURES = "%s/tests/fixtures/pages" %(CODE_ROOT)

WHM_DOCKER_CONTAINER_NAME = "whm"

TIMEOUT_BEFORE_STARTING_TESTS = 15

STEP_COUNTER = 0

print("-------------------------")
print("Setting up test fixtures")
print("-------------------------")

if (os.path.isdir(CODE_TEST_FIXTURES) == False):
	sys.exit("Problem: test fixtures directory missing \nSolution: Looks like you might need to run `git pull`\n") 

result = subprocess.run("docker ps --no-trunc --format \"{{.Mounts}}\"", shell=True, stdout=subprocess.PIPE)
if CODE_TEST_FIXTURES in str(result.stdout):
	print("Test container is already running!")
	TIMEOUT_BEFORE_STARTING_TESTS = 1
else:
	print("Test container NOT running!")

	# Stop the Docker before copying the tests, this will help avoid permission problems
	if(WHM_DOCKER_CONTAINER_NAME in str(subprocess.check_output(['docker' ,'ps']))):
		STEP_COUNTER += 1
		print("\n{}) Attempting to stop WHM Grav container".format((STEP_COUNTER)))
		os.system("docker-compose stop")

	STEP_COUNTER += 1
	print("\n{}) Attempting to start WHM Grav container".format(STEP_COUNTER))	
	os.system("docker-compose -f docker-run-tests.yml up -d")

# Wait until the container is accessible, wait 10 seconds
STEP_COUNTER += 1
print("\n{}) Starting tests in {} seconds".format(STEP_COUNTER,TIMEOUT_BEFORE_STARTING_TESTS))
print("\n=======================================================")
time.sleep(TIMEOUT_BEFORE_STARTING_TESTS)
subprocess.call("./vendor/bin/codecept run --steps --html --debug", shell=True)