import sys
import os
import glob
import dropbox
from dropbox.files import WriteMode
from dropbox.exceptions import ApiError, AuthError
import shutil
from pprint import pprint

TOKEN = ''
DBX = {}


def most_recently_created_backup():
    enteries = DBX.files_list_folder('').entries
    
    # Don't consider the dev backup as the latest
    enteries = list(filter(lambda s: s.name != "whm_dev_backup.zip", enteries))
    enteries.sort(key=lambda s: s.server_modified, reverse=True)

    if (len(enteries) < 1):
        print("Error, unable to determine the most recently created backup")
        
    print("This file has been determined as the most recently created backup\n\t %s" %(enteries[0]))

    return enteries[0]
    
def download_from_dropbox(file_to_download, download_to_file):

    print("Attempting to download {} to {}".format(file_to_download.path_lower, download_to_file))

    try:
        DBX.files_download_to_file(download_to_file, file_to_download.path_lower, None)
    except ApiError as e:
        sys.exit("ERROR: Unable to download file\n{}".format(err))


def download_most_recent_backup():
    BACKUP_DOWNLOAD_LOCATION = "/tmp/"
    CODE_ROOT = "%s" %(os.getcwd())
    CODE_GRAV = "%s/grav" %(CODE_ROOT)
    BACKUP_DOWNLOAD_LOCATION = "/tmp/"

    STEP_COUNTER = 0

    print("Download most recent backup")
    print("================================")
    # if (os.path.isdir(CODE_GRAV) == True):
    #     sys.exit("Problem: ./grav/ already exists. To prevent potential data loss we exit")

    STEP_COUNTER += 1
    print("    \n{}) Download most recent backup".format((STEP_COUNTER)))
    backup_file = most_recently_created_backup()
    BACKUP_DOWNLOAD_LOCATION = "{}{}".format(BACKUP_DOWNLOAD_LOCATION, backup_file.name)

    download_from_dropbox(backup_file, BACKUP_DOWNLOAD_LOCATION)

    BACKUP_FOLDER_LOCATION = BACKUP_DOWNLOAD_LOCATION.replace(".zip", "")

    STEP_COUNTER += 1
    if (os.path.isdir(BACKUP_FOLDER_LOCATION) == True):
        print("    \n{}) Deleting extraction folder".format((STEP_COUNTER)))
        try:
            shutil.rmtree(BACKUP_FOLDER_LOCATION)
        except OSError as e:
            sys.exit("   - Unable to delete : %s - %s." % (e.filename, e.strerror))

    STEP_COUNTER += 1
    print("    \n{}) Creating extraction folder {}".format(STEP_COUNTER, BACKUP_FOLDER_LOCATION))
    os.mkdir(BACKUP_FOLDER_LOCATION)

    STEP_COUNTER += 1
    print("    \n{}) Decompressing {} to extraction folder {}".format(STEP_COUNTER, BACKUP_DOWNLOAD_LOCATION, BACKUP_FOLDER_LOCATION))
    if (os.system("unzip -q {} -d {}".format(BACKUP_DOWNLOAD_LOCATION, BACKUP_FOLDER_LOCATION)) > 0):
        sys.exit("   - Unable to extract zip file.")

    STEP_COUNTER += 1
    print("    \n{}) Creating .grav folder".format(STEP_COUNTER))
    os.makedirs(CODE_GRAV)

    def copy_folder(src, dest):
        try:
            shutil.copytree(src, dest)
        # Directories are the same
        except shutil.Error as e:
            print('Directory not copied. Error: %s' % e)
        # Any error saying that the directory doesn't exist
        except OSError as e:
            print('Directory not copied. Error: %s' % e)

    STEP_COUNTER += 1
    print("    \n{}) copying user/accounts to .grav".format(STEP_COUNTER))
    copy_folder("{}/user/accounts".format(BACKUP_FOLDER_LOCATION), "{}/user/accounts".format(CODE_GRAV))

    STEP_COUNTER += 1
    print("    \n{}) copying user/config to .grav".format(STEP_COUNTER))
    copy_folder("{}/user/config".format(BACKUP_FOLDER_LOCATION), "{}/user/config".format(CODE_GRAV))

    STEP_COUNTER += 1
    print("    \n{}) copying user/pages to .grav".format(STEP_COUNTER))
    copy_folder("{}/user/pages".format(BACKUP_FOLDER_LOCATION), "{}/user/pages".format(CODE_GRAV))

    STEP_COUNTER += 1
    print("    \n{}) copying user/data to .grav".format(STEP_COUNTER))
    copy_folder("{}/user/data".format(BACKUP_FOLDER_LOCATION), "{}/user/data".format(CODE_GRAV))

    STEP_COUNTER += 1
    print("    \n{}) copying user/plugins to .grav".format(STEP_COUNTER))
    copy_folder("{}/user/plugins".format(BACKUP_FOLDER_LOCATION), "{}/user/plugins".format(CODE_GRAV))

    STEP_COUNTER += 1
    print("    \n{}) copying plugins to .grav".format(STEP_COUNTER))
    copy_folder("{}/logs".format(BACKUP_FOLDER_LOCATION), "{}/logs".format(CODE_GRAV))

if __name__ == '__main__':
    
    if (len(sys.argv) < 2):
        sys.exit("ERROR: Missing the command line parameter for TOKEN")

    TOKEN = sys.argv[1]

    if (len(TOKEN) == 0):
        sys.exit("ERROR: We don't have an access token for DropBox. ")

    DBX = dropbox.Dropbox(TOKEN)

    # Check that the access token is valid
    try:
        DBX.users_get_current_account()
    except AuthError as err:
        sys.exit("ERROR: Invalid access token; try re-generating an "
            "access token from the app console on the web.")

    download_most_recent_backup()