import os
import sys
import shutil
import subprocess
import time

def download_most_recent_dev_backup():
	CODE_ROOT = "%s" %(os.getcwd())
	CODE_GRAV = "%s/grav" %(CODE_ROOT)
	CODE_GRAV_PAGES = "%s/user/pages" %(CODE_GRAV)
	CODE_GRAV_USER = "%s/user" %(CODE_GRAV)
	BACKUP_DOWNLOAD_LOCATION = "/tmp/whm_dev_backup.zip"
	UNZIP_BACKUP_LOCATION = "/tmp/whm_dev_backup"
	BACKUP_PAGES_FOLDER = "/tmp/whm_dev_backup/user/pages"

	STEP_COUNTER = 0

	print("Download most recent dev backup")
	print("================================")
	
	if (os.path.isdir(CODE_GRAV_PAGES)):
		shutil.rmtree(CODE_GRAV)

	STEP_COUNTER += 1
	print("    \n{}) Download most recent Dev backup".format((STEP_COUNTER)))
	os.system("curl -L -o {} https://www.dropbox.com/s/ig7pszzkiqfh5sz/whm_dev_backup.zip?dl=0".format(BACKUP_DOWNLOAD_LOCATION))

	STEP_COUNTER += 1
	print("    \n{}) Deleting extraction folder if it exists".format((STEP_COUNTER)))
	try:
		if (os.path.isdir(UNZIP_BACKUP_LOCATION)):
			shutil.rmtree(UNZIP_BACKUP_LOCATION)
	except OSError as e:
	    sys.exit("   - Unable to delete : %s - %s." % (e.filename, e.strerror))

	STEP_COUNTER += 1
	print("    \n{}) Creating extraction folder {}".format(STEP_COUNTER, UNZIP_BACKUP_LOCATION))
	os.mkdir(UNZIP_BACKUP_LOCATION)

	STEP_COUNTER += 1
	print("    \n{}) Decompressing {} to extraction folder {}".format(STEP_COUNTER, BACKUP_DOWNLOAD_LOCATION, UNZIP_BACKUP_LOCATION))
	os.system("unzip -q {} -d {}".format(BACKUP_DOWNLOAD_LOCATION, UNZIP_BACKUP_LOCATION))

	STEP_COUNTER += 1
	print("    \n{}) Creating .grav folder".format(STEP_COUNTER))
	os.makedirs(CODE_GRAV_USER)

	STEP_COUNTER += 1
	print("    \n{}) Copying backup {} to dev env {}".format(STEP_COUNTER, BACKUP_PAGES_FOLDER,CODE_GRAV_PAGES))
	try:
	    shutil.copytree(BACKUP_PAGES_FOLDER, CODE_GRAV_PAGES)
	# Directories are the same
	except shutil.Error as e:
	    print('Directory not copied. Error: %s' % e)
	# Any error saying that the directory doesn't exist
	except OSError as e:
	    print('Directory not copied. Error: %s' % e)

def compile_sass():
	STEP_COUNTER = 0

	print("Compiling SASS into CSS")
	print("================================")

	STEP_COUNTER += 1
	print("    \n{}) Download most recent Dev backup".format((STEP_COUNTER)))
	os.system("grunt sass")

if __name__ == '__main__':
	WHM_DOCKER_CONTAINER_NAME = "whm"
    
	print("---------------------------")
	print("Setting up dev environment")
	print("---------------------------")

	# Stop the Docker before copying the tests, this will help avoid permission problems
	if(WHM_DOCKER_CONTAINER_NAME in str(subprocess.check_output(['docker' ,'ps']))):
		print("Attempting to stop WHM Grav container")
		os.system("docker-compose stop")

	print("\n\n")

	download_most_recent_dev_backup()
	compile_sass()
