#!/bin/bash
phash=$(echo '<?php echo password_hash(getenv("ADMIN_PASSWORD"), PASSWORD_DEFAULT); ?>' | php)

cat >${NGINX_WEBROOT}/user/accounts/${ADMIN_USERNAME}.yaml <<EOF
email: $ADMIN_EMAIL
access:
  admin:
    login: true
    super: true
  site:
    login: true
fullname: $ADMIN_FULLNAME
hashed_password: $phash
EOF
