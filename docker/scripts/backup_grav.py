import sys
import os
import shutil
import glob
import dropbox
from dropbox.files import WriteMode
from dropbox.exceptions import ApiError, AuthError
import json
from pprint import pprint

TOKEN = ''

KEEP_THIS_MANY_BACKUPS = 10
BACKUP_DIR = ''

DBX = {}

def most_recently_created_backup():
    all_backups = [s for s in os.listdir(BACKUP_DIR)
         if os.path.isfile(os.path.join(BACKUP_DIR, s))]
    all_backups.sort(key=lambda s: os.path.getmtime(os.path.join(BACKUP_DIR, s)), reverse=True)

    if (len(all_backups) < 1):
        print("Error, unable to determine the most recently created backup")
        sys.exit

    print("This file has been determined as the most recently created backup %s" %(all_backups[0]))

    pprint(all_backups[0])

    return all_backups[0]

def delete_backup_file(backup_file):
    print("Deleting uploaded backup file %s" %(backup_file))
    try:
        os.remove(backup_file)
	except OSError as e:
	    sys.exit("   - Unable to delete : %s - %s." % (e.filename, e.strerror))

# Uploads contents of LOCALFILE to Dropbox
def upload_to_dropbox(backup_file_absolute):

    localfile = backup_file_absolute
    print("Local file to be uploaded to DropBox %s" %(localfile))

    with open(localfile, 'rb') as f:
        file_size = os.path.getsize(localfile)
        CHUNK_SIZE = 4 * 1024 * 1024

        upload_session_start_result = DBX.files_upload_session_start(f.read(CHUNK_SIZE))
        cursor = dropbox.files.UploadSessionCursor(session_id=upload_session_start_result.session_id,
                                                   offset=f.tell())
        commit = dropbox.files.CommitInfo(path="/" + os.path.basename(backup_file_absolute), mode=dropbox.files.WriteMode.overwrite)

        print("Starting Upload...")
        while f.tell() < file_size:
            if ((file_size - f.tell()) <= CHUNK_SIZE):
                DBX.files_upload_session_finish(f.read(CHUNK_SIZE),
                                                cursor,
                                                commit)
                print ("Uploading... 100%")
                print("Successfully uploaded")
            else:
                DBX.files_upload_session_append(f.read(CHUNK_SIZE),
                                                cursor.session_id,
                                                cursor.offset)
                print ("Uploading...{} %".format(100 * float(f.tell())/float(file_size)))
                cursor.offset = f.tell()

def clean_up_unwanted_backups():
    enteries = DBX.files_list_folder('').entries
    if (len(enteries) < KEEP_THIS_MANY_BACKUPS):
        print ("No unwanted backups to remove")
        return

    # Sort files by server_modified date newest first
    enteries.sort(key=lambda x:x.server_modified, reverse=True)

    for i in range(KEEP_THIS_MANY_BACKUPS, len(enteries)):
        try:
            DBX.files_delete(enteries[i].path_display)
        except ApiError as err:
             sys.exit("ERROR: Delete file")
    

def create_dev_backup():
    WHM_DEV_BACKUP_TMP = "/tmp/whm_dev_backup_tmp/"
    WHM_DEV_BACKUP = "/tmp/whm_dev_backup/"
    WHM_DEV_BACKUP_ZIP = "/tmp/whm_dev_backup.zip"

    WHM_DEV_BACKUP_TMP_PAGES_DIR = "{}user/pages".format(WHM_DEV_BACKUP_TMP)
    WHM_DEV_BACKUP_PAGES_DIR = "{}user/pages".format(WHM_DEV_BACKUP)

    # Delete old archive if it exists
    if (os.path.isfile(WHM_DEV_BACKUP_ZIP)):
        try:
            print("Old zip detected, attempting to delete {}".format(WHM_DEV_BACKUP))
            os.remove(WHM_DEV_BACKUP_ZIP)
        except OSError as e:  ## if failed, report it back to the user ##
            print ("Error: %s - %s." % (e.filename, e.strerror))

    if (os.path.isdir(WHM_DEV_BACKUP)):
        try:
            print("Attempting to delete {}".format(WHM_DEV_BACKUP))
            shutil.rmtree(WHM_DEV_BACKUP)
        except OSError as e:
            print ("   - Unable to delete : %s - %s." % (e.filename, e.strerror))

    if (os.path.isdir(WHM_DEV_BACKUP_TMP)):
        try:
            print("Attempting to delete {}".format(WHM_DEV_BACKUP_TMP))
            shutil.rmtree(WHM_DEV_BACKUP_TMP)
        except OSError as e:
            print ("   - Unable to delete : %s - %s." % (e.filename, e.strerror))

    print("Creating {} and {}".format(WHM_DEV_BACKUP, WHM_DEV_BACKUP_TMP))
    os.makedirs(WHM_DEV_BACKUP)
    os.makedirs(WHM_DEV_BACKUP_TMP)

    print("Unziping backup to {}".format(WHM_DEV_BACKUP_TMP))
    recent_backup = most_recently_created_backup()
    os.system("unzip -q {} -d {}".format(BACKUP_DIR + recent_backup, WHM_DEV_BACKUP_TMP))

    print("Copying the pages folder {} to {}".format(WHM_DEV_BACKUP_TMP_PAGES_DIR, WHM_DEV_BACKUP_PAGES_DIR))
    try:
        shutil.copytree(WHM_DEV_BACKUP_TMP_PAGES_DIR, WHM_DEV_BACKUP_PAGES_DIR)
    # Directories are the same
    except shutil.Error as e:
        print('Directory not copied. Error: %s' % e)
    # Any error saying that the directory doesn't exist
    except OSError as e:
        print('Directory not copied. Error: %s' % e)


    print("Creating zip {}".format(WHM_DEV_BACKUP_ZIP))
    os.chdir(WHM_DEV_BACKUP)
    os.system("cd {} && zip -r {} {}".format(WHM_DEV_BACKUP, WHM_DEV_BACKUP_ZIP, "."))

    upload_to_dropbox(WHM_DEV_BACKUP_ZIP)



if __name__ == '__main__':
    
    if (len(sys.argv) < 2):
        sys.exit("ERROR: Missing the command line parameters for TOKEN and BACKUP_DIR")

    TOKEN = sys.argv[1]
    BACKUP_DIR = sys.argv[2]

    print(BACKUP_DIR)

    if (len(TOKEN) == 0):
        sys.exit("ERROR: We don't have an access token for DropBox. ")
    if (os.path.isdir(BACKUP_DIR) == False):
        sys.exit("ERROR: Backup directory doesn't exist")

    DBX = dropbox.Dropbox(TOKEN)

    # Check that the access token is valid
    try:
        DBX.users_get_current_account()
    except AuthError as err:
        sys.exit("ERROR: Invalid access token; try re-generating an "
            "access token from the app console on the web.")

    clean_up_unwanted_backups()
    backup_file = BACKUP_DIR + most_recently_created_backup()
    upload_to_dropbox(backup_file)
    create_dev_backup()
    delete_backup_file(backup_file)